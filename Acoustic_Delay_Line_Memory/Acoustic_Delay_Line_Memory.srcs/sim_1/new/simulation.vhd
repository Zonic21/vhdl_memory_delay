----------------------------------------------------------------------------------
-- Company: FH technikum wien
-- Engineer: Nilles Kevin
-- 
-- Create Date: 21.01.2017 11:15:07
-- Design Name: delay_module
-- Module Name: dlyline_mem - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: vivado 2016.4
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity simulation is

end simulation;

architecture Behavioral of simulation is

component dlyline_mem is
  port(
    s_clk_i   : in  std_logic;                    -- 100 MHz clock (coming from board clock oscillator)
    s_pb1_n_i : in  std_logic;                    -- low-active push button PUSH1 (reset button)
    s_pb4_n_i : in  std_logic;                    -- low-active push button PUSH4 ("write command")
    s_sw_i    : in  std_logic_vector(7 downto 0); -- 8 DIL switches ("write data")
    s_rcv_i   : in  std_logic;                    -- input from ultrasonic transducer (receiver)
    s_xmt_o   : out std_logic;                    -- output to ultrasonic transducer (sender)
    s_xmt_n_o : out std_logic;                    -- inverted output to ultrasonic transducer (sender) 
    s_led_o   : out std_logic_vector(7 downto 0)  -- 8 LEDs (show "read data")
    );
end component;
			signal clk_i   : std_logic := '0';                    -- 100 MHz clock (coming from board clock oscillator)
            signal pb1_n_i : std_logic := '0';                    -- low-active push button PUSH1 (reset button)
            signal pb4_n_i : std_logic := '0';                    -- low-active push button PUSH4 ("write command")
            signal sw_i    : std_logic_vector(7 downto 0) :="00000000"; -- 8 DIL switches ("write data")
            signal rcv_i   : std_logic;                    -- input from ultrasonic transducer (receiver)
            signal xmt_o   : std_logic;                    -- output to ultrasonic transducer (sender)
            signal xmt_n_o : std_logic := '0';                    -- inverted output to ultrasonic transducer (sender) 
            signal led_o   : std_logic_vector(7 downto 0) :="00000000";  -- 8 LEDs (show "read data")	
            		
begin 
  i_dlyline_mem : dlyline_mem
  port map              
    (
            s_clk_i         =>          clk_i,
            s_pb1_n_i       =>          pb1_n_i,
            s_pb4_n_i       =>          pb4_n_i,
            s_sw_i          =>          sw_i,
            s_rcv_i         =>          rcv_i,
            s_xmt_o         =>          xmt_o,
            s_xmt_n_o       =>          xmt_n_o,
            s_led_o         =>          led_o
	);
	
	rcv_i <= transport xmt_o after 9020us; --(8.72ms + 300us);  9020us
	
  -- Generate clock
  -- in diesem prozess wird der 100Mhz clock generiert.
      Clk0 : process
      begin
          clk_i <= '0';
      wait for 5 ns;
          clk_i <= '1';
      wait for 5 ns;
      end process Clk0;
-- dies ist der Prozess der eigentlichen simulation.       
 p_run : process 
      begin
      -- die buttons und switches werden in die start position gelegt. und daraufhin der hard reset aktiviert.
          pb1_n_i <= '1';
          wait for 50 ns;
          sw_i <= (others => '0');
          pb1_n_i <= '0';             -- activation hard reset
          wait for 50 ns;             -- after reset
          pb1_n_i <= '1';
          
          sw_i <= "10101010"; --erste schalter position 0xAA
          wait for 10ms;      
          pb4_n_i <= '0';     -- reinladen der werte in das system
          wait for 10ms;
          pb4_n_i <= '1';     -- schleife der momentanen bits im system
          wait for 100ms;
          sw_i <= "11110000"; -- neuer wert f�r das system an den PB 0xF0
          wait for 20ms;
          pb4_n_i <= '0';     -- reinladen der neuen werte in das system     
          wait for 10ms; 
          wait;
                                                          -- stop simulation
          assert false report "END OF SIMULATION" severity error;
      end process p_run;     
      
end Behavioral;
