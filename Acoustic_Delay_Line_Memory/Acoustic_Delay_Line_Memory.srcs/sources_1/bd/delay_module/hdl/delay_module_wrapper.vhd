----------------------------------------------------------------------------------
-- Company: FH technikum wien
-- Engineer: Nilles Kevin
-- 
-- Create Date: 21.01.2017 11:15:07
-- Design Name: delay_module
-- Module Name: dlyline_mem - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: vivado 2016.4
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;


entity dlyline_mem is
  port(
    s_clk_i   : in  std_logic;                    -- 100 MHz clock (coming from board clock oscillator)
    s_pb1_n_i : in  std_logic;                    -- low-active push button PUSH1 (reset button)
    s_pb4_n_i : in  std_logic;                    -- low-active push button PUSH4 ("write command")
    s_sw_i    : in  std_logic_vector(7 downto 0); -- 8 DIL switches ("write data")
    s_rcv_i   : in  std_logic;                    -- input from ultrasonic transducer (receiver)
    s_xmt_o   : out std_logic;                    -- output to ultrasonic transducer (sender)
    s_xmt_n_o : out std_logic;                    -- inverted output to ultrasonic transducer (sender) 
    s_led_o   : out std_logic_vector(7 downto 0)  -- 8 LEDs (show "read data")
    );
end dlyline_mem;

architecture Behavioral of dlyline_mem is

signal s_rcv_i_1                    : std_logic; -- first input register
signal s_rcv_i_2                    : std_logic; -- second input register
signal s_bit                        : std_logic; -- signalisiert ein detectiertes Bit
signal s_bitsync                    : std_logic; -- ein mit Bittime synchronisiertes bit
signal s_bitxmt                     : std_logic; -- momentaner bit wert oder neuer wert von den dip switches
signal s_bittime                    : std_logic; -- gibt den takt vor wann das momentane bit verarbeitet wird
signal s_bitcnt                     : std_logic_vector(2 downto 0); --dieser counter gibt das momentane bit an welches gerade verarbeitet wird
signal s_burst                      : std_logic; -- die 15 * 40khz burst signale um eine 1 zu signalisieren

signal burstcount                   : std_logic_vector(10 downto 0); -- counter der eine halbe periode des burst z�hlt 1250 = 100 1110 0010
signal burstdelaycount              : std_logic_vector(13 downto 0); -- counter der das delay zwischen bittime und burst z�hlt 15000 = 11 1010 1001 1000
signal bittimecount                 : std_logic_vector(16 downto 0); -- counter der den takt des bittime vorgibt 125000 = 1 1110 1000 0100 1000
signal burstnumbercount             : std_logic_vector(4 downto 0); -- z�hlt bei jeder burst periode hoch bis 15 burst signale erzeugt wurden
signal loadbit                      : std_logic; -- signal zwischen MUX1 und MUX2 wo der momentane dip switch wert anliegt
signal s_bittime_buffer             : std_logic; -- bittime register welches zum LED register geht



begin

-- prozess der counter fuer das bittime und den burst
Bittime_counter : process (s_clk_i,s_pb1_n_i,bittimecount, burstdelaycount, burstnumbercount, burstcount)	
begin
if (s_pb1_n_i = '0') then        -- hardreset , zur�cksetzen aller signale                        
	burstcount <= (others => '0');
	burstdelaycount <= (others => '0');
	bittimecount <= (others => '0');
	burstnumbercount <= (others => '0');
	s_burst    <= '0';
elsif (rising_edge(s_clk_i)) then	
    -- taktgeber f�r den bittime counter 1,25ms takt
    bittimecount <= bittimecount + 1;
    if (bittimecount = "11110100001001000") then
        s_bittime <= '1';
        bittimecount <= (others => '0');
        burstdelaycount <= burstdelaycount + 1;
    else
        s_bittime <= '0';
    end if;
    -- damit der burst keine 16 mal z�hlt
    if (burstnumbercount <= "00000") then
        s_burst <= '0';
    end if;  
    -- 150us delay zeit zwischen bittime und dem start des burst. 
    if (burstdelaycount = "11101010011000") then
        burstdelaycount <= (others => '0');
        burstnumbercount <= burstnumbercount + 1;
    elsif(burstdelaycount >= "00000000000001") then
        burstdelaycount <= burstdelaycount + 1;
    end if;
    if (burstnumbercount >= 1) then
        burstcount <= burstcount + 1;
    end if;
    -- counter der das 40 khz signal countet
    if (burstcount = "10011100010") then
        s_burst <= not s_burst;
        burstnumbercount <= burstnumbercount + 1;
        burstcount <= (others => '0');
    end if; 

end if;
end process Bittime_counter;
-- counter fuer den bitcount damit das system weiss welches das momentane bit im system ist. (8bit)
Bit_counter : process (s_clk_i,s_pb1_n_i, s_bittime, s_bitcnt)	
begin
if (s_pb1_n_i = '0') then         -- hardreset , zur�cksetzen aller signale 
    s_bitcnt <= (others => '0');
elsif (rising_edge(s_clk_i)) then	

    if(s_bittime = '1') then
        s_bitcnt <= s_bitcnt + 1;
    end if;   
end if;
end process Bit_counter;
-- der multiplexter schaltet den Dipswitch wert durch der dem momentanen bitcount wert entspricht (8switches)
MUX1 : process (s_clk_i,s_pb1_n_i, s_bitcnt, loadbit, s_sw_i)	
begin
if (s_pb1_n_i = '0') then         -- hardreset , zur�cksetzen aller signale 
	loadbit <= '0';
elsif (rising_edge(s_clk_i)) then	
        case s_bitcnt is
            when "000" =>   loadbit <= s_sw_i(0);
            when "001" =>   loadbit <= s_sw_i(1);
            when "010" =>   loadbit <= s_sw_i(2);
            when "011" =>   loadbit <= s_sw_i(3);
            when "100" =>   loadbit <= s_sw_i(4);
            when "101" =>   loadbit <= s_sw_i(5);
            when "110" =>   loadbit <= s_sw_i(6);
            when "111" =>   loadbit <= s_sw_i(7);
            when others =>   loadbit <= '0';
        end case;  

end if;
end process MUX1;
-- mit dem pushbutton 4 kann man einen neuen wert in das speichersystem laden.
MUX2 : process (s_clk_i,s_pb1_n_i, s_pb4_n_i, loadbit, s_bitsync)	
begin
if (s_pb1_n_i = '0') then         -- hardreset , zur�cksetzen aller signale 
	s_bitxmt <= '0';
elsif (rising_edge(s_clk_i)) then	
        case s_pb4_n_i is
            when '0' =>   s_bitxmt <= loadbit;
            when '1' =>   s_bitxmt <= s_bitsync;
            when others =>   s_bitxmt <= '0';
        end case;  
end if;
end process MUX2;
-- das von MUx 2 durchgeschaltete signal ist die zu sendende 0 oder 1, so wird entweder eine 0 oder das burst signal durchgeschaltet
MUX3 : process (s_clk_i,s_pb1_n_i,s_burst)	
begin
if (s_pb1_n_i = '0') then         -- hardreset , zur�cksetzen aller signale 
	s_xmt_o <= '0';
	s_xmt_n_o <= '0';
elsif (rising_edge(s_clk_i)) then	
        if (s_bitxmt = '1') then
            s_xmt_o <= s_burst;
            s_xmt_n_o <= not s_burst;
        else
            s_xmt_o <= '0';
            s_xmt_n_o <= '1';       
        end if;
end if;
end process MUX3;
-- synchronisiert das vom bit detection register output signal mit dem bittime
Bittime_sync_register : process (s_clk_i,s_pb1_n_i, s_bittime, s_bit)	
begin
if (s_pb1_n_i = '0') then         -- hardreset , zur�cksetzen aller signale 
	s_bitsync <= '0';
elsif (rising_edge(s_clk_i)) then	
        if (s_bittime = '1') then
            s_bitsync <= s_bit;
        end if;
end if;
end process Bittime_sync_register;
-- das 40 khz burst signal wird detektiert und somit eine 1, das register wandelt die burst signale in ein eindeutiges signal um
Bit_detect_register : process (s_clk_i, s_pb1_n_i, s_rcv_i, s_rcv_i_1, s_bittime, s_rcv_i_2)	
begin
if (s_pb1_n_i = '0') then           -- hardreset , zur�cksetzen aller signale 
	s_bit <= '0';
	s_rcv_i_1 <= '0';
	s_rcv_i_2 <= '0';
elsif (rising_edge(s_clk_i)) then	
        s_rcv_i_1 <= s_rcv_i;
        s_rcv_i_2 <= s_rcv_i_1;
        if (s_bittime = '1') then
            s_bit <= '0';
        elsif ((s_bittime = '0') and (s_rcv_i_2 = '1')) then
            s_bit <= '1';
        end if;
end if;
end process Bit_detect_register;
-- das LED register zeigt die momentane im system vorhandene bit konstellation an. diese wird permanent aktuel gehalten somit wird direkt eine �nderung angezeigt
led_registers : process (s_clk_i, s_pb1_n_i, s_bittime, s_bittime_buffer, s_bitcnt)	
begin
if (s_pb1_n_i = '0') then           -- hardreset , zur�cksetzen aller signale 
	s_led_o <= (others => '0');
	s_bittime_buffer <= '0';
elsif (rising_edge(s_clk_i)) then	
    s_bittime_buffer <= s_bittime;
    if (s_bittime_buffer = '1') then
        case s_bitcnt is
            when "000" =>   s_led_o(0) <= s_bitsync;
            when "001" =>   s_led_o(1) <= s_bitsync;
            when "010" =>   s_led_o(2) <= s_bitsync;
            when "011" =>   s_led_o(3) <= s_bitsync;
            when "100" =>   s_led_o(4) <= s_bitsync;
            when "101" =>   s_led_o(5) <= s_bitsync;
            when "110" =>   s_led_o(6) <= s_bitsync;
            when "111" =>   s_led_o(7) <= s_bitsync;
            when others =>   s_led_o <= (others => '0');
        end case;   
    end if;
end if;
end process led_registers;


end Behavioral;
